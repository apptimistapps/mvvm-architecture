//
//  PostManager.swift
//  MVVMProject
//
//  Created by Dee Odus on 27/01/2022.
//

import Foundation
class PostManager{
	
	let posts: [Post]
	
	init(posts: [Post]){
		
		self.posts = posts
	}
	
	func getPostsFrom(index: Int) -> Post{
		
		return posts[index]
	}
}
