//
//  Post.swift
//  MVVMProject
//
//  Created by Dee Odus on 27/01/2022.
//

import Foundation

struct Post: Decodable{
	
	let title: String
	let body: String
}
