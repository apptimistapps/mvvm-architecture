//
//  PostTableViewCell.swift
//  MVVMProject
//
//  Created by Dee Odus on 27/01/2022.
//

import UIKit

final class PostTableViewCell: UITableViewCell {

	//making them private allows better composition
	@IBOutlet weak private var titleLabel: UILabel!
	@IBOutlet weak private var bodyLabel: UILabel!
	
	
	func setUpView(postViewModel: PostViewModel, index: Int){
	
		titleLabel.text = postViewModel.addNumberingToTitle(index: index)
		bodyLabel.text = postViewModel.addSymbolToBody()
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
