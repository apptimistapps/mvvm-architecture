//
//  APIService.swift
//  MVVMProject
//
//  Created by Dee Odus on 27/01/2022.
//

import Foundation

final class APIService {
	
	private init() { }
	
	static let shared = APIService()
	
	func fetchPosts(urlString: String, completion: @escaping ([Post]?, Error?) -> Void){
		
		guard let url = URL.init(string: urlString) else { return }
		
		let task = URLSession.shared.dataTask(with: url) { data, response, error in
			
			guard let data = data else { return }
			
			do{
				
				let decoder = JSONDecoder()
				let json = try decoder.decode([Post].self, from: data)
				completion(json, nil)
			}
			catch let err {
				completion(nil, err as NSError)
			}
		}
		
		task.resume()
		
	}
	
}
