//
//  PostViewModel.swift
//  MVVMProject
//
//  Created by Dee Odus on 27/01/2022.
//

import Foundation

final class PostViewModel{
	
	private let post: Post
	
	init(post: Post){
		self.post = post
	}
	
	func addNumberingToTitle(index: Int) -> String{
		
		"\(index + 1). \(post.title)"
	}
	
	func addSymbolToBody() -> String{
		
		"\(["$", "@", "*", "£", "%"].randomElement() ?? "@") \(post.body)" 
	}
}
