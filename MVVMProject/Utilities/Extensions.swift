//
//  Extensions.swift
//  MVVMProject
//
//  Created by Dee Odus on 27/01/2022.
//

import Foundation
import UIKit

extension UITableViewCell{
	
	static var identifier: String{
		
		return String(describing: self)
	}
}
