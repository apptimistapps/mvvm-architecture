//
//  PostViewController.swift
//  MVVMProject
//
//  Created by Dee Odus on 27/01/2022.
//

import UIKit

final class PostViewController: UIViewController {

	@IBOutlet weak private var postTableView: UITableView!
	let datasource = PostDataSource()
	
    override func viewDidLoad() {
        super.viewDidLoad()

		postTableView.dataSource = datasource
		datasource.delegate = self
		datasource.fetchPosts()
    }
    
}

extension PostViewController: PostDelegate{
	
	func didFetchPost(posts: [Post]) {
		
		//perform other actions if the posts count == 0
		DispatchQueue.main.async {
			
			self.postTableView.reloadData()
		}
	}
	
	
}
