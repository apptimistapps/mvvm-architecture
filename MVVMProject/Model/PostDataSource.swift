//
//  PostDataSource.swift
//  MVVMProject
//
//  Created by Dee Odus on 27/01/2022.
//

import Foundation
import UIKit

protocol PostDelegate: AnyObject{
	
	func didFetchPost(posts: [Post])
}

final class PostDataSource: NSObject{
	
	private var posts: [Post]?
	weak var delegate: PostDelegate?
	
	func fetchPosts(){
		
		APIService.shared.fetchPosts(urlString: StringLiterals.postAPIUrl) { posts, error  in
			
			if error == nil{
				
				guard let posts = posts else { return }
				self.posts = posts
				self.delegate?.didFetchPost(posts: posts)
			}
			else{
				
				print(error?.localizedDescription ?? "")
			}
		}
	}
	
}

extension PostDataSource: UITableViewDataSource{
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		guard let posts = posts else { return 0 }
		return posts.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: PostTableViewCell.identifier, for: indexPath) as! PostTableViewCell
		
		guard let posts = posts else { fatalError() }
		
		let post = posts[indexPath.row]
		cell.setUpView(postViewModel: PostViewModel.init(post: post), index: indexPath.row)
		
		return cell
	}
	
	
	
}
